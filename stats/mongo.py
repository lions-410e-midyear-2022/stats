from collections import Counter, defaultdict
from datetime import datetime
import os

import clubs
import excel

from attrs import asdict, define
from dotenv import load_dotenv
from pymongo import MongoClient
from rich import print

load_dotenv()

client = MongoClient(
    host=os.getenv("MONGODB_HOST"),
    port=int(os.getenv("MONGODB_PORT")),
    username=os.getenv("MONGODB_USERNAME"),
    password=os.getenv("MONGODB_PASSWORD"),
)
db = client["lions_410e_2023_midyear"]
collection = db["reg_form"]


def toggle_deleted(reg_num, deleted):
    d = collection.find_one({"reg_num": reg_num})
    d["deleted"] = deleted
    collection.replace_one({"reg_num": reg_num}, d)


@define
class Attendee:
    reg_num: int
    name: str
    name_badge: str
    club: str
    member_type: str
    dietary: str
    disability: str
    first_names: str
    last_name: str
    email: str


@define
class Deleted:
    reg_num: int
    name: str


@define
class Stats:
    total_attendees: int = 0
    lions: int = 0
    non_lions: int = 0
    clubs: Counter = None
    dietary: list = None
    disability: list = None
    attendees: list = None
    deleted: list = None

    def populate(self, collection=None):
        records = list(collection.find())
        self.clubs = Counter()
        self.dietary = []
        self.disability = []
        self.attendees = []
        self.deleted = []
        for record in records:
            print(record)
            if not record.get("deleted", False):
                if record["lion"]:
                    self.lions += 1
                else:
                    self.non_lions += 1
                self.clubs[record["club"]] += 1
                locals = {}
                for attr in ("dietary", "disability"):
                    if record[attr] and record[attr].lower().strip(".,; ") not in (
                        "none",
                        "no",
                        "non",
                        "n/a",
                        "na",
                        "nil",
                        "not applicable",
                    ):
                        getattr(self, attr).append(record[attr])
                        locals[attr] = record[attr]
                self.attendees.append(
                    Attendee(
                        record["reg_num"],
                        record["full_name"],
                        record["name_badge"],
                        record["club"],
                        "Lion" if bool(record["lion"]) else "PIS/Child",
                        locals.get("dietary", "None"),
                        locals.get("disability", "None"),
                        record["first_names"],
                        record["last_name"],
                        record["email"],
                    )
                )
            else:
                self.deleted.append(Deleted(record["reg_num"], record["full_name"]))

        self.total_attendees = self.lions + self.non_lions

    def make_excel(self, file_name="district_410e_2023_midyear_conference_stats.xlsx"):
        wb = excel.ExcelWorkbook(datetime.now())
        wb.add_sheet(
            f"Attendees",
            [
                "Reg Num",
                "Name",
                "Name Badge",
                "Club",
                "Lion or PIS/Child",
                "Dietary Requirements",
                "Disability Requirements",
            ],
            widths={
                "Reg Num": 20,
                "Name": 40,
                "Name Badge": 40,
                "Club": 30,
                "Lion or PIS/Child": 30,
                "Dietary Requirements": 50,
                "Disability Requirements": 50,
            },
        )
        for attendee in self.attendees:
            wb.add_row(list(asdict(attendee).values())[:-5])
        wb.add_sheet("Totals", ["Item", "Total"], widths={"Item": 30, "Total": 20})
        wb.add_row(["Total Attendees", self.total_attendees])
        wb.add_row(["Lions", self.lions])
        wb.add_row(["Non-Lions", self.non_lions])
        wb.save(file_name)


if __name__ == "__main__":
    stats = Stats()
    stats.populate(collection)
    print(stats)
    stats.make_excel()
    # print(
    #     ";".join(
    #         [
    #             attendee.email
    #             for attendee in stats.attendees
    #             if attendee.district == "410E"
    #         ]
    #     )
    # )
    # # print(stats)
