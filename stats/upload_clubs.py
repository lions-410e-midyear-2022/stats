import os

from dotenv import load_dotenv
from pymongo import MongoClient
from rich import print

import clubs

load_dotenv()

client = MongoClient(
    host=os.getenv("MONGODB_HOST"),
    port=int(os.getenv("MONGODB_PORT")),
    username=os.getenv("MONGODB_USERNAME"),
    password=os.getenv("MONGODB_PASSWORD"),
)
db = client["lions_410e_2022_midyear"]
collection = db["clubs"]
# collection = db["reg_form"]


def insert_clubs():
    try:
        for (club, district) in clubs.CLUBS.items():
            d = {"club": club, "district": district}
            print(f"Inserting {d}")
            collection.insert_one(d)
    except Exception:
        pass


def get_district_clubs(district):
    print(district)
    for club in collection.find({"district": district}):
        print(club)


# insert_clubs()
get_district_clubs("410E")
