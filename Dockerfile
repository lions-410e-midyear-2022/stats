FROM registry.gitlab.com/kimvanwyk/python3-flask-poetry:main

COPY ./stats/*.py /app/
COPY ./stats/templates/*.html /app/templates/
ENV MODULE_NAME=app
ENV MONGODB_HOST=FILL_ME
ENV MONGODB_PORT=FILL_ME
ENV MONGODB_USERNAME=FILL_ME
ENV MONGODB_PASSWORD=FILL_ME
